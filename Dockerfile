FROM python:2.7-alpine
LABEL maintainer="mail@m4rcu5.nl"

COPY ./app/ /app
RUN pip install -r /app/requirements.txt

EXPOSE 5000
WORKDIR /app
CMD ["python", "app.py"]
