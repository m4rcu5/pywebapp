# Python based web service
----
Dockerized Python web service built with Flask    

## Requirements
* Docker (tested on version 1.13.1)

## Set up
1. Build your Docker image    
`docker build -t your_app_name .`    

1. Start your container    
`docker run --rm -d -p 5000:5000 your_app_name:latest`

## Usage
Curl example:

    >_ curl http://127.0.0.1:5000
    Welcome!

    >_ curl http://127.0.0.1:5000/slack/beertime
    {
      "response_type": "ephemeral", 
      "text": "Nope!"
    }

###### ToDo
* Add SSL support
* World domination
* Find life in space
