'''
    The main request handler of the app
'''
import datetime
import pytz
from flask import Flask, jsonify

APP = Flask(__name__)

@APP.route('/', methods=['GET'])
def main():
    '''
        The default answer
    '''
    return 'Welcome!'

@APP.route('/slack/beertime', methods=['GET'])
def beertime():
    '''
    Check if it's time for beer yet. (Fri 16:30 - Mon 00:00)

    Request: curl http://127.0.0.1:5000/slack/beertime
    Response:
    {
      "response_type": "ephemeral",
      "text": "Nope!"
    }

    '''
    d = datetime.datetime.now(tz=pytz.timezone('Europe/Amsterdam'))
    d_time = d.time()
    if d.isoweekday() in range(1, 5) \
        or (d.isoweekday() is 5 and (d_time <= datetime.time(16, 30))):
        return jsonify(
            response_type='ephemeral',
            text='Nope!'
            )
    else:
        return jsonify(
            response_type='ephemeral',
            text='Cheers!'
            )

if __name__ == '__main__':
    # Set to 0.0.0.0 to make it work in Docker container
    APP.run(host='0.0.0.0')
